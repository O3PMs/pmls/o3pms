#+TITLE: O3PMs :: Objects for Project, Programm and Portfolio Management system
#+AUTHOR: Eugene Yukhno
#+DATE: 2023-02-01 

#+TODO: BackLog(t) inDev(p) NeedUpdate(u) | Done(d) Canceled(c) OnHold(h) 

* Intruduction
  <TBD>

** Authors

+ *Eugene Yukhno* ([[yukhno@gmail.com]])

  _About me_: 
  <TBD>

  _My roles in project_:
  <TBD>

*** Copyright

Copyright (c) 2023 Eugene Yukhno ([[yukhno@gmail.com]])

*** License

Licensed under the MIT License.


* Software Development
  <TBD>

** Programming Aproach

*** inDev README.org

*** inDev o3pms.asd
    Define Common Lisp programm system. Define dependences and path

*** BackLog doc/UserGuide.tex

*** BackLog doc/AdminGuide.tex

*** BackLog doc/PML/*
    Subproject -- Developemnt Project Meta Language

*** BackLog src/settings.lisp
    All settings. Define all global objects and constatns

*** inDev src/core.lisp
    Main classes
    [[file:src/core.lisp]]

*** BackLog [#C] src/cli.lisp
    Implementation O3PM Shell

*** BackLog [#C] src/server.lisp
    Implementation O3PM Server

*** BackLog [#B] src/pmlc.lisp
    PML Compiler (Project Meta Language)

*** BackLog [#C] src/fsdb.lisp
    Implemenation File System Data Base subsystem

*** BackLog [#C] src/kb.lisp
    Implement KB like Methodology subsystem

*** BackLog [#C] src/bpms.lisp
    Implement BPMS based on states (Petry Net)

*** BackLog [#B] src/tex.lisp
    Connetion with LaTeX

*** BackLog [#A] src/tj.lisp
    Connection with TaskJuggler

*** BackLog [#A] src/tw.lisp
    Connection with TaskWarrior

*** BackLog [#C] src/tt.lisp
    Connection with TimeTracker

*** BackLog [#A] src/lib/o3pm-datatime.lisp



** System Architecture
   + Description
   + etc

*** /CLI/ :: O3PM Shell -- Command Line Interface
    Командный процессо может подключиться к серверу O3PM. При помощи комманд можно
    выполнять все действия связанные с управлением проектами, программами проектов
    или портфелем проектов. А тажке можно выполнять все работы админские задачи.
    /CLI/ будет работать как клиент, который может присоединяться к серверу
    напрямую по протоколу ssh или присоединяться к /ESB/ через котору сможет
    соединиться с сервером.

    /CLI/ будет построен по принципу графического эмулятора терминало 
    (like =xterm=, etc.) Помоимо того у /CLI/ должна быть функция автодополнения 
    команд, истории команд, индикации статуса соединения и т.п. 

*** /SRV/ :: *O3PM Server -- Server organize interfaces and execute commands*
    <TBD>

*** /PMLC/ :: *O3PM Compiler -- Compile PML*
    <TBD>

*** /TeX/ :: *O3PM Reporting and Documents Templates subsystems*
    <TBD>

*** Use Exist Solutions /ESB/ :: *Enterprise Service Bus*
    Нет необходимости создавать собственную систему, которая реализует функции
    корпоративной системной шины. Вместо этого предлагается выбрать несколько
    популярных готовых реализаций и использовать их "как есть".

    Ниже приведены готове решения, которых хотелось бы испльзовать в порядке
    приоритетности и важдности:

**** [#A] Rabbit MQ
     <TBD>

**** [#A] Apache Kafka
     <TBD>



* Cases

** Business Cases
   <TBD>

*** Use Case :: <>
    <TBD>
 
