;;;; -*- Mode: lisp; Syntax: Common-Lisp; indent-tabs-mode: nil -*-

;;;
;;; o3pm-datetime.lisp
;;; ---------
;;; This file contain the description of core O3PM's classes   
;;;

#|

  This file is a part of help project.
  Copyright (c) 2023 Eugene Yukhno (yukhno@gmail.com)

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use, copy,
  modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

|#


(in-package :o3pms)


(defconstant +days-in-month+ #(31 28 31 30 31 30 31 31 30 31 30 31))
(defconstant +days+4w-in-month+ #(3 0 3 2 3 2 3 3 2 3 2 3))
(defconstant +shortname-of-month+
  #("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))

(defconstant +name-of-month+
  #("January" "February" "March" "April" "May" "June"
    "July" "August" "September" "October" "November" "December"))

(defconstant +shordays-of-week #("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat"))
(defconstant +2char-days-of-week+ #("Su" "Mo" "Tu" "We" "Th" "Fr" "Sa"))
(defconstant +days-of-week+
  #("Sanday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday"))


(defun dt-weekday (sym)
  (let* ((ls (read-dt-symbol sym))
         (year (car ls))
         (month (cadr ls))
         (day (caddr ls)))
    (decf month 2)
    (when (< month 3)
      (decf year)
      (incf month 12))
    (mod (+ day year
            (floor year 4)
            (floor year 400)
            (- (floor year 100))
            (floor (* 31 month) 12))
         7)))
  
(defun read-dt-symbol (dts)
  (let* ((dt-str (symbol-name dts))
         (year (read-from-string dt-str t t :start 0 :end 4))
         (month (read-from-string dt-str t t :start 5 :end 7))
         (day (read-from-string dt-str t t :start 8 :end 10)))
    (list year month day)))

(defun read-delta-symbol (ds)
  (let* ((d-str (symbol-name ds))
         (ds-len (length d-str))
         (res (list 0 0 0)))
    (let ((l1 (read-from-string d-str t nil :start (- ds-len 1) :end ds-len))
          (d1 (read-from-string d-str t nil :start 0 :end (- ds-len 1)))
          (l2 "")
          (d2 0))
      (if (> ds-len 2)
          (progn
            (setf l2 (read-from-string d-str t nil :start (- ds-len 2) :end ds-len))
            (setf d2 (read-from-string d-str t nil :start 0 :end (- ds-len 2)))))
      (when (eq l1 'y) (setf (car res) d1))
      (when (eq l2 'mo) (setf (cadr res) d2))
      (when (eq l1 'w) (setf (caddr res) (* 7 d1)))
      (when (eq l1 'd) (setf (caddr res) d1)))
    res))
         

(defun make-dt-symbol (dtl)
  (read-from-string
   (format nil "~4,'0d-~2,'0d-~2,'0d" (car dtl) (cadr dtl) (caddr dtl))))

(defun dt< (ldt rdt)
  (let* ((lldt (read-dt-symbol ldt))
         (lrdt (read-dt-symbol rdt))
         (nldt (+ (* (car lldt) 10000) (* (cadr lldt) 100) (caddr lldt)))
         (nrdt (+ (* (car lrdt) 10000) (* (cadr lrdt) 100) (caddr lrdt))))
    (if (< nldt nrdt) t nil)))

(defun dt> (ldt rdt)
  (dt< rdt ldt))


(defun dt= (ldt rdt)
  (let* ((lldt (read-dt-symbol ldt))
         (lrdt (read-dt-symbol rdt)))
    (if (and (= (car lldt) (car lrdt))
             (= (cadr lldt) (cadr lrdt))
             (= (caddr lldt) (caddr lrdt)))
        t nil)))



(defun dt+ (dt dd)
  (let ((ldt (read-dt-symbol dt))
        (ldd (read-delta-symbol dd)))
    (multiple-value-bind
          (m d) (floor (+ (caddr ldt) (caddr ldd)) 30)
      (setf (caddr ldt) d)
      (incf (cadr ldt) m))
    (multiple-value-bind
          (y m) (floor (+ (cadr ldt) (cadr ldd)) 12)
      (setf (cadr ldt) m)
      (incf (car ldt) y))
    (incf (car ldt) (car ldd))
    ldt))
