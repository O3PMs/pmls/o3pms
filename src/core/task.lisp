;;;; -*- Mode: lisp; Syntax: Common-Lisp; indent-tabs-mode: nil -*-

;;;
;;; task.lisp
;;; ---------
;;; This file contain the description of core O3PM's classes:
;;;   class milestone
;;;   class task
;;;

#|

  This file is a part of help project.
  Copyright (c) 2023 Eugene Yukhno (yukhno@gmail.com)

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use, copy,
  modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

|#

(in-package :o3pms)


#| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Milestone
   ---------

   The special trivial o3pm's objects (a particular form of task)
   The duration of the milestone equals 0 always, :start = :finish
   
   Milestone hasn't the assignment -- because permanently assigned to
   the current project manager.
   
   Milestones haven't the task-specific calendar -- because a milestone
   will be calculated depending on predecessors, or had constraints directly
   ignore the calendar

   Milestone's slots:

    name                         ; the short name of o3pm's object 
      :initarg :name                  ; uses (make-instace 'milestone :name "...")
      :accessor name                  ; uses for get|set (o3pm-name <object>)
      :initform "<milestone>"         ; set default value => "<milestone>"

    id                           ; for autogenerate ID of o3pm's object
      :initarg :id                    ; uses (make-instance 'milestone :id <id value>)
      :accessor id                    ; uses for get|set (id <object>)
      :initform nil                   ; set default value => () is empty list

    ptr                          ; the list of milestone's properties
      :accessor properties            ; uses for get|set (properties <object>)
      :initform ...                   ; set by default nil
                                      ; but milestone's properties initialize by
                                      ; the instance constructor method

      Milestone's properties
        :date              => { (ASAP)        - As Soon As Possible
                                (ALAP)        - As Soon As Late
                                (MBON <date>) - Must Be On <date>
                                (NETH <date>) - Not Earlier Than <date> }
        :deadline          => <date> -- Not Laiter Than <date>
        :depends-on        => list of predecessors (it is a list of obj's pointers)
        :hidden            => if milestone will not show on project scheduler
        :show-on-summary   => if t then milestone will have copying on summary block
        :completed         => completed flag for manual mark
        :active-if         => alway if equal 't' or list with depend on milestones
        :completed-date    => it is actual completed date
        :paid-after-completed

    calc
      :accessor calc-result
      :initform ...

      Milestone's calculated attributs
        :need-recalc       => true :: all other calculated attributs will be recalc
        :start             => equal max of predecessors :finish plus the lag
        :finish            => equal :start

   Milestone's methods:
     (gen-id ms-obj)           ; for autoincremental generate ID like MS-00001
     (calc ms-obj)             ; for calculation
     (getptr ms-obj key)       ; get value of 'key' property
     (setptr ms-obj key val)   ; set value of 'key' property
     (getcalc ms-obj key)      ; get calculation property
     (setcalc ms-obj key val)  ; set calculation property


|#

(defclass milestone (o3pm) ())


;; Milestone's ID counter
(let ((ms-cval 0))
  (defun ms-counter () (incf ms-cval))
  (defun get-ms-couter () ms-cval))


(defmethod gen-id ((obj milestone))
  (read-from-string (format nil "MS-~5,'0d" (ms-counter))))


;; Milestone Constructor method
(defmethod initialize-instance
    :around ((obj milestone) &key (name "<milestone>") (date nil) (id nil))
  
  (unless id (setf id (gen-id obj)))
  (call-next-method obj :name name :id id)
  
  (setptr obj :constraints-type 'ASAP)
  (setptr obj :depends-on nil)
  (setptr obj :hidden nil)
  (setptr obj :completed nil)
  (setptr obj :active-if t)
  
  (setcalc obj :start nil)
  (setcalc obj :finish nil))


;; Milestone's calculation method
(defmethod calc ((obj milestone))
  ; Milestone Calculation:
  ;   :start = max(predecessors->finish + link-lags)
  ;   :finish = :start
  
  ; Get all predecessors finish date
  (dolist (p-obj (car (getf (properties obj) :depends-on)))
    (format t "~%MILESTONE::CALC > p-obj = ~a" p-obj)
    (let ((p-finish (getf (calc-result (eval p-obj)) :finish)))
      (format t "~%MILESTONE::CALC > p-obj::calc = ~a" (calc-result (eval p-obj)))
      (format t "~%MILESTONE::CALC > p-obj::calc > :finish = ~a" p-finish)
      (let ((o-start (getcalc obj :start)))
        (setcalc obj :start p-finish)
        (setcalc obj :finish p-finish)))))
    

    

#| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Task
   ----

   Task is inhirited from milestone
   It has duration. Has assigments and task-specific calendar

   Task's slots:

    name                         ; the short name of o3pm's object 
      :initarg :name                  ; (make-instace 'milestone :name "...")
      :accessor name                  ; make accessor (o3pm-name <object>)
      :initform "<milestone>"         ; for set default value => "<milestone>"

    id                           ; for autogenerate ID of o3pm's object
      :initarg :id                    ; (make-instance 'milestone :id <id value>)
      :accessor id                    ; make accessor (id <object>)
      :initform nil                   ; default value => () is empty list

    ptr
      :accessor properties
      :initform ...

      Task's properties
        :depends-on        => list of predecessors
        :hidden            => if milestone will not show on project scheduler
        :show-on-summary   => if t then milestone will have copying on summary block
        :completed         => completed flag for manual mark
        :active-if         => alway if equal 't' or list with depend on milestones
        :start             => { (ASAP) - As Soon As Possible
                                (ALAP) - As Late As Possible
                                (MBON <date>) - Must Be/Started On <date>
                                (NETH <date>) - Start Not Earlier Than <date>
                                (NLTH <date>) - Start Not Laiter Than <date> }
                                                (default value is 'ASAP)
                               ((ASAP)) - ok
                               ((ALAP)) - ok
                               ((MBON)) - ok
                               ((NETH <date>)) - ok
                               ((NLTH <date>)) - ok
                               ((NETH <date>) (NLTH <date>)) - ok
                               ((ASAP) (NETH <date>)) - ok!
                               ((ALAP) (NETH <date>)) - ok!
                               ((ALAP) (ASAP)) - ok! => ASAP
                               ((MBON) (...)) - ok! => MBON
                               ((ASAP) (NLTH <date>)) - ok!
                               ((ALAP) (NLTH <date>)) - ok!
        :finish            => { (ASAP)
                                (ALAP)
                                (MBON <date>)
                                (NETH <date>)
                                (NLTH <date>) } (default value is 'ASAP)
        :priority          => 0..1000; 0 - High Priority; 1000 - Low Priority; uses for
                                       automatical resouces collision solving
        :fixed-work        => it is flag (default value is True)
        :fixed-units nil 
        :hammock nil
        :can-be-interrupted t

    calc
      :accessor calc-result
      :initform ...

      Task's calculated attributs:
        :need-recalc       => true :: all other calculated attributs will be recalc
        :start             => equal max of predecessors :finish plus the lag
        :finish            => equal :start

        :deadline
        :actual-start nil)
        :actual-finish nil)
        :actual-duration nil)
        :actual-cost nil)
        :actual-work nil)
        :actual-work-time-scale nil)
        :actual-cost-time-scale nil)
        :actual-start nil)
        :actual-finish nil)
        :actual-duration nil)
        :actual-cost nil)
        :acutal-work nil)
        :actual-work-time-scale nil)
        :actual-cost-time-scale nil)
        :%work-completed nil)

    calendar
      :accessor calendar
      :initform nil

    assigments
      :accessor assign
      :initform nil

   Task's methods:
     (gen-id ms-obj)           ; for autoincremental generate ID like TT-00001
     (calc ms-obj)             ; for calculation
     (getptr ms-obj key)       ; get value of 'key' property
     (setptr ms-obj key val)   ; set value of 'key' property
     (getcalc ms-obj key)      ; get calculation property
     (setcalc ms-obj key val)  ; set calculation property

|#

(defclass task (milestone)
  ((calendar
    :accessor calendar
    :initform nil)
   (assigments
    :accessor assign
    :initform nil)))

   
;; Task ID's counter
(let ((tsk-cval 0))
  (defun tsk-counter () (incf tsk-cval))
  (defun get-tsk-couter () tsk-cval))


(defmethod gen-id ((obj task))
  (read-from-string (format nil "TT-~5,'0d" (tsk-counter))))


;; Task Constructor method 
(defmethod initialize-instance :around ((obj task) &key (name "<task>") (id nil))
  
  (unless id (setf id (gen-id obj)))
  (call-next-method obj :name name :id id)
  
  (setptr obj :priority 1000)
  (setptr obj :fixed-work t)
  (setptr obj :fixed-units nil)
  (setptr obj :hammock nil)
  (setptr obj :can-be-interrupted t)
  (setptr obj :actual-work-time-scale nil)
  (setptr obj :actual-cost-time-scale nil)
  (setptr obj :actual-start nil)
  (setptr obj :actual-finish nil)
  (setptr obj :actual-duration nil)
  (setptr obj :actual-cost nil)
  (setptr obj :acutal-work nil)
  (setptr obj :actual-work-time-scale nil)
  (setptr obj :actual-cost-time-scale nil)
  (setptr obj :%work-completed nil)
  
  (setcalc obj :actual-start nil)
  (setcalc obj :actual-finish nil)
  (setcalc obj :actual-duration nil)
  (setcalc obj :actual-cost nil)
  (setcalc obj :actual-work nil)
  (setcalc obj :duration nil)
  (setcalc obj :work nil)
  (setcalc obj :work-time-scale nil)
  (setcalc obj :cost nil)
  (setcalc obj :cost-time-scale nil)
  (setcalc obj :%time-completed nil)
  (setcalc obj :%work-completed nil)
  (setcalc obj :remaining-work nil)
  (setcalc obj :remaining-duration nil)
  (setcalc obj :remaining-cost nil)
  (setcalc obj :ACWP nil)
  (setcalc obj :BCWP nil)
  (setcalc obj :BCWS nil)
  (setcalc obj :CV nil)
  (setcalc obj :SV nil)
  (setcalc obj :CPI nil)
  (setcalc obj :SPI nil))

(defmethod calc ((obj task))
  ; Task Calculation:
  ;   :start = max(predecessors->finish + link-lags)
  ;   :finish = :start + :duration
  
  ; Get all predecessors finish date
  (dolist (p-obj (car (getf (properties obj) :depends-on)))
    (format t "~TASK::CALC > p-obj = ~a" p-obj)
    (let ((p-start (getcalc p-obj :start))
          (p-finish (getcalc p-obj :finish)))
      (setcalc obj :start p-finish)
      (setcalc obj :finish (getcalc obj :duration)))))
    
