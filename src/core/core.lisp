;;; -*- Mode: lisp; Syntax: Common-Lisp; indent-tabs-mode: nil -*-

;;;
;;; core.lisp
;;; ---------
;;; This file contain the description of core O3PM's classes   
;;;

#|

  This file is a part of help project.
  Copyright (c) 2023 Eugene Yukhno (yukhno@gmail.com)

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use, copy,
  modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

|#

(in-package :o3pms)


#| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Abstract basic class 'O3PM'
   ---------------------------
|#

(defclass o3pm ()
  ((name                         ; the short name of o3pm's object 
    :initarg :name                  ; key for set init value (make-instace :name "...")
    :accessor name                  ; make accessor (o3pm-name <object>)
    :initform "<short name>")       ; for set default value => "<short name>"
   (id                           ; for autogenerate ID of o3pm's object
    :initarg :id                    ; can usage: (make-instance :id <some id value>)
    :accessor id                    ; make accessor (id <object>)
    :initform nil)                  ; default value => () is empty list
   (ptr :accessor properties :initform nil)      ; list of user defined properties
   (calc :accessor calc-result :initform nil)))  ; list of calculated properties


(defgeneric gen-id (obj))           ; for autogenerate ID
(defgeneric calc (obj))             ; for calculation
(defgeneric getptr (obj key))       ; get value of 'key' property
(defgeneric setptr (obj key val))   ; set value of 'key' property
(defgeneric getcalc (obj key))      ; get calculation property
(defgeneric setcalc (obj key val))  ; set calculation property


(defmethod getptr ((obj o3pm) key)
  (getf (slot-value obj 'ptr) key))

(defmethod setptr ((obj o3pm) key val)
  (setf (getf (slot-value obj 'ptr) key) val))

(defmethod getcalc ((obj o3pm) key)
  (getf (slot-value obj 'calc) key))

(defmethod setcalc ((obj o3pm) key val)
  (setf (getf (slot-value obj 'calc) key) val))


