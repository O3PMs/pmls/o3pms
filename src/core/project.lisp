;;;; -*- Mode: lisp; Syntax: Common-Lisp; indent-tabs-mode: nil -*-

;;;
;;; project.lisp
;;; ---------
;;; This file contain the description of core O3PM's classes:
;;;    class workpackage (task)
;;;    class wbs
;;;    class wp-wbs (wbs)
;;;    class project ()
;;;

#|

  This file is a part of help project.
  Copyright (c) 2023 Eugene Yukhno (yukhno@gmail.com)

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use, copy,
  modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

|#

(in-package :o3pms)



#| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   WorkPackage
   -----------

   The WorkPackage is inhirited from task
   It has WBS (work breakdown structure) -- hirarhical list of tasks and milestones
   which speciafied statement of works

   The WorkPackage is a summary task or block of tasks which has all task's properties
   and one additional -- wbs = statement of work | sub-tasks

   WorkPagckage's slots:

    name                         ; the short name of o3pm's object 
      :initarg :name                  ; (make-instace 'workpackage :name "...")
      :accessor name                  ; make accessor (o3pm-name <object>)
      :initform "<milestone>"         ; for set default value => "<milestone>"

    id                           ; for autogenerate ID of o3pm's object
      :initarg :id                    ; (make-instance 'workpackage :id <id value>)
      :accessor id                    ; make accessor (id <object>)
      :initform nil                   ; default value => () is empty list

    ptr
      :accessor properties
      :initform ...

      Task's properties
        :depends-on        => list of predecessors
        :hidden            => if workpackage will not show on project scheduler
        :show-on-summary   => if t then workpackage will have copying on summary block
        :completed         => completed flag for manual mark
        :active-if         => alway if equal 't' or list with depend on milestones
        :start             => { (ASAP) - As Soon As Possible
                                (ALAP) - As Late As Possible
                                (MBON <date>) - Must Be/Started On <date>
                                (NETH <date>) - Start Not Earlier Than <date>
                                (NLTH <date>) - Start Not Laiter Than <date> }
                                                (default value is 'ASAP)
                               ((ASAP)) - ok
                               ((ALAP)) - ok
                               ((MBON)) - ok
                               ((NETH <date>)) - ok
                               ((NLTH <date>)) - ok
                               ((NETH <date>) (NLTH <date>)) - ok
                               ((ASAP) (NETH <date>)) - ok!
                               ((ALAP) (NETH <date>)) - ok!
                               ((ALAP) (ASAP)) - ok! => ASAP
                               ((MBON) (...)) - ok! => MBON
                               ((ASAP) (NLTH <date>)) - ok!
                               ((ALAP) (NLTH <date>)) - ok!
        :finish            => { (ASAP)
                                (ALAP)
                                (MBON <date>)
                                (NETH <date>)
                                (NLTH <date>) } (default value is 'ASAP)
        :priority          => 0..1000; 0 - High Priority; 1000 - Low Priority; uses for
                                       automatical resouces collision solving
        :fixed-work        => it is flag (default value is True)
        :fixed-units nil 
        :hammock nil
        :can-be-interrupted t

    calc
      :accessor calc-result
      :initform ...

      WorkPackage's calculated attributs:
        :need-recalc       => true :: all other calculated attributs will be recalc
        :start             => 
        :finish            => 

        :deadline
        :actual-start nil)
        :actual-finish nil)
        :actual-duration nil)
        :actual-cost nil)
        :actual-work nil)
        :actual-work-time-scale nil)
        :actual-cost-time-scale nil)
        :actual-start nil)
        :actual-finish nil)
        :actual-duration nil)
        :actual-cost nil)
        :acutal-work nil)
        :actual-work-time-scale nil)
        :actual-cost-time-scale nil)
        :%work-completed nil)

    calendar
      :accessor calendar
      :initform nil

    assigments
      :accessor assign
      :initform nil

    wbs
      :reader wbs
      :initform nil

   WorkPackage's methods:
     (gen-id wp-obj)           ; for autoincremental generate ID like WP-00001
     (calc wp-obj)             ; for calculation
     (getptr wp-obj key)       ; get value of 'key' property
     (setptr wp-obj key val)   ; set value of 'key' property
     (getcalc wp-obj key)      ; get calculation property
     (setcalc wp-obj key val)  ; set calculation property

     (get-wbs-item wp-obj key)
     (add-wbs-item wp-obj wbs-obj)
     (del-wbs-item wp-obj key)
     (link-wbs-item wp-obj wbs-obj link-dsl-string predecessor-obj)


|#

(defclass workpackage (task)
  ((wbs
    :reader wbs
    :initform nil)))



(defgeneric get-wbs-item (obj key))
(defgeneric add-wbs-item (obj wbs-obj))
(defgeneric del-wbs-item (obj key))



;; WorkPackage's ID counter
(let ((wp-cval 0))
  (defun wp-counter ()
    (incf wp-cval))
  (defun get-wp-couter ()
    wp-cval))


(defmethod id-gen ((obj workpackage))
  (setf (o3pm-id obj)
        (read-from-string (format nil "WP-~5,'0d" (wp-counter)))))


;; WorkPackage Constructor method
(defmethod initialize-instance
    :around ((obj workpackage)
             &key (name "<wp>") (id nil))
  (unless id (setf id (gen-id obj)))
  (call-next-method obj :name name :id id))


;; WorkPackage methods implementation
(defmethod add-wbs-item ((obj workpackage) wbs-obj)
  (if (or (eq (type-of wbs-obj) 'milestone)
          (eq (type-of wbs-obj) 'task))
      (when wbs-obj
        (setf (getf (slot-value obj 'wbs) (id wbs-obj)) wbs-obj))
      nil))     ; WorkPackage can include 'milestone' or 'task' only


(defmethod get-wbs-item ((obj workpackage) key)
  (getf (slot-value obj 'wbs) key))


(defmethod del-wbs-item ((obj workpackage) key)
  (remf (slot-value obj 'wbs) key))






#| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Project
   -------

   Проект должен проверять уникальность ID в рамках себя
   Проект содержит минимум 2 BaseLine: Estimation и еще один
   В Estimation попадает все что делалось при планировании (до начала работ по проекту)
   Как только будет внесены первые отчеты о работе (началось выполнение проекта),
   Estimation отмечатеся как потерявший актуальность и автоматически создается
   (или выбирается созданный) еще один ВL Actual где и проводятся все расчёты
   в связи с зависимостями.
   По желанию пользователя может быть создано неограничеснное к-во BaseLine
   В каждом BaseLine автоматически создается суммарный рабочий пакет проекта

   В проекте может быть несколько статусных дней отмеченных спициальным Milestone

   WBS любого проектного BaseLine может включать в себя Task и Milestones
|#

(defclass project (workpackage)
  (baselines))

(defgeneric load-pml (obj fn))
(defgeneric save-pml (obj fn))


;; Task Project's counter
(let ((prj-cval 0))
  (defun prj-counter ()
    (incf prj-cval))
  (defun get-prj-couter ()
    prj-cval))


(defmethod id-gen ((obj project))
  (setf (o3pm-id obj)
        (read-from-string (format nil "PJ-~5,'0d" (prj-counter)))))





(defmethod load-pml ((obj project) fn)
  nil)

(defmethod save-pml ((obj project) fn)
  nil)

