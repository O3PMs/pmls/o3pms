;;;; -*- Mode: lisp; Syntax: Common-Lisp; indent-tabs-mode: nil -*-

;;;
;;; o3pms.asd --- ...
;;;

#|

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use, copy,
  modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

|#

(defsystem "o3pms"
  :version "0.1.0"
  :author "Eugene Yukhno"
  :license "MIT"
  :depends-on ("local-time")
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "pmlc")
                 (:module "core"
                  :components
                  ((:file "core")
                   (:file "task")
                   (:file "project"))))))
  :description ""
  :in-order-to ((test-op (test-op "o3pms/test"))))


(defsystem "o3pms/test"
  :author "Eugene Yukhno"
  :license "MIT"
  :depends-on ("o3pms"
               "rove")
  :components ((:module "test"
                :components
                ((:file "main"))))
  :description "Test system for o3pms"
  :perform (test-op (op oc) (symbol-call :rove :run c)))
