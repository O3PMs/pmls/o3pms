(o3pm-define :entity :project
	     (o3pm-define :property :timezone)
	     (o3pm-define :property :currency))
;; (:project
;;   (:timezone nil
;;    :currency nil))

(project
 (:id "O3PM Systems"
  :alias "o3pm"
  :timezone "Atlantic/Madeira"
  :currency "EUR"
  :dataformat "yyyy-mm-dd"
  :timeformat "hh:mm"
  :planning-from "start"
  :start "2022-07-27"
  :baselines
      (:init nil
       :actual nil)))
