;; -*- PML v0.1 -*-
;; If version pre-processor intruction not define, then use last version which can use you PML-compliller

;; Automatical include Global.pml for global settings with Iterative Waterfall methodology
;; If you need use something else, then you can redefine all entities and include that


what if now 2022-08-01	;; instruction for what if analysis -- calculate


Project "O3PM Systems"
  ''' O3PM mind Project, Programm and Portfolio Management System '''


  alias o3pm		 	;; short name for link with this project
  timezone Atlantic/Madeira     ;; default value is "Atlantic/Madeira"
  currency EUR                  ;; default value is "Euro"
  dateforamt yyyy-mm-dd         ;; default value is ISO YYYY-MM-DD
  timeformat hh:mm              ;; default value is 24h HH:MM with 0 => 09:00

  planning from start		;; default planning form start
  start at 2022-07-27           ;; if start not define -- compiler automaticly add start at first compile date


  baseline init			;; initial planning baseline (is default)
  baseline actual		;; actual timescale (is default)



  ;; -= Define local project based resources =- 

  Resources
    Work "Eugene Yukhno"
      alias EY
      email yukhno@gmail.com
      phone +351 925 147 311
      rate 100 EUR
      limits
        capacity 10h per week
	      availabel
          from 17:00 until 23:00
          or from 04:30 until 07:30



  ;; -= Work Breakdown Structure =-
  
  WorkPackage "Project Management"
    ''' This workpackage for organize and accounting all PM activities '''
    
    WorkPackage "Milestones & Deliveries"
      Milestone "Formal Start"
        alias prjstart
	      start at #o3pm:start	;; properties start define at o3pm.start property values
	      show on timeline

	  WorkPackage "Workshops & Meetings"
    WorkPackage "Periodical Tasks"


  WorkPackage "PML Development"
    WorkPackage "PML v0.1" alias PML01
      ''' v0.1 -- Initial version of Project Meta Language
                  This version implement the basic pricipals '''

    WorkPackage "PML v0.2"
      ''' v0.2 -- Next version of PML '''
      
      alias PML02
      start #PML01:finish
      
      
  WokrPackage "O3PMs Developemnt"
    WorkPackage Architecture
    WorkPackage "SWD Planning"
    WorkPackage "SWD Iterations"
    WorkPackage "O3PM Tests"


  WorkPackage "Self Deployment"
  
    ;; Start definition the local macros for this workpackage

    Define Macro SelfDeployment
      parameters
        default
	      subalias

      WorkPackage #main alias #subalias
        Task "Deploy [default]"
	      alias "[#subalias]-deploy

	      Task "Review standard libraries with [default]"
	      Task "Make new issues for next version"

     ;; end of macros definition


    SelfDeployment "Version 0.1" alias v0.1   ;; Use macros with parameters 
